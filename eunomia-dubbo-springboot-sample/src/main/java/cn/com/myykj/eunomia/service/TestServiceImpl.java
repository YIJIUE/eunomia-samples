package cn.com.myykj.eunomia.service;

import cn.com.myykj.GatewayService;
import org.apache.dubbo.config.annotation.Service;


@GatewayService
@Service
public class TestServiceImpl implements TestService {

    @Override
    public String hello(String name) {
        return "ok " + name;
    }

}
