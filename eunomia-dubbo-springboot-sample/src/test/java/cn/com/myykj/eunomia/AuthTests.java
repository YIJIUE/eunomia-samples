package cn.com.myykj.eunomia;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.stream.Collectors;

@SpringBootTest
public class AuthTests {

    @Test
    public void providerSign(){
        HashMap<String, Object> map = new HashMap<>(2);
        long l = System.currentTimeMillis();
        map.put("timestamp", l);//1581501793804
        map.put("appSecret", "aa0638d4a634471d9bb56ebaf6f9c7b7");// 在eunomia界面上申请的 appSecret

        String unencryptedSign = map.keySet()
                .stream()
                .sorted(Comparator.naturalOrder())
                .map(key -> StringUtils.join(key, map.get(key)))
                .collect(Collectors.joining()).trim();

        System.out.println("timestamp: " + l);
        System.out.println("sign: " + DigestUtils.md5DigestAsHex(unencryptedSign.getBytes()));
    }
}
